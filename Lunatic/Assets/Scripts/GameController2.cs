﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;  // Need to using/import UI package

public class GameController2 : MonoBehaviour {
	
	public static int pickedUp = 0;
	
	int needed = 6; 
	public Text scoreText ;
	
	public GameObject door;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		scoreText.text = pickedUp.ToString();// + " / " + needed.ToString();
		
		
		if (pickedUp > needed) {
			
			GameObject.Destroy(door);
		}
	}
}
