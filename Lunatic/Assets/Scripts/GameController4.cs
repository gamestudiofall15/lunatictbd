﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;  // Need to using/import UI package

public class GameController4 : MonoBehaviour {

	public static int pickedUp = 0;
	
	int needed = 1; 
	public Text scoreText ;

    public GameObject door;

	// Update is called once per frame
	void Update () {

		if (pickedUp > needed) {

			scoreText.text = "OBJECTIVE : RETURN TO SHIP";

			GameObject.Destroy(door);

		}
	}
}
