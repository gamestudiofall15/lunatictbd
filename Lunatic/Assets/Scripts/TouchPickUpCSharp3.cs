﻿using UnityEngine;
using System.Collections;

public class TouchPickUpCSharp3 : MonoBehaviour {

	void OnTriggerEnter ( Collider other) {
		
		// You touched an object give player a point	
		GameController3.pickedUp = GameController3.pickedUp + 1;
	
		Debug.Log ("Pick Up");

		// Remove this gameobject attached to this script
		Destroy(this.gameObject);
	}
	
}
