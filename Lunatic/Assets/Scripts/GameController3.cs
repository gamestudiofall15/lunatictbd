﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;  // Need to using/import UI package

public class GameController3 : MonoBehaviour {

	public static int pickedUp = 0;
	
	int needed = 0; 
	public Text scoreText ;

	public GameObject door;
	public GameObject HelmetLight;
	public GameObject HelmetLight02;


	// Update is called once per frame
	void Update () {

		if (pickedUp > needed) {

			GameObject.Destroy(HelmetLight);
			GameObject.Destroy(HelmetLight02);
			scoreText.text = "OBJECTIVE : ESCAPE";

			GameObject.Destroy(door);

		}
	}
}
