﻿using UnityEngine;
using System.Collections;

public class TouchItemPickUp2 : MonoBehaviour {
	
	void OnTriggerEnter ( Collider other) {
		
		// You touched an object give player a point	
		GameController2.pickedUp = GameController2.pickedUp + 1;
		
		Debug.Log ("Pick Up");
		
		// Remove this gameobject attached to this script
		Destroy(this.gameObject);
	}
}

