﻿using UnityEngine;
using System.Collections;

public class TouchPickUpCSharp : MonoBehaviour {
	
	void OnTriggerEnter ( Collider other) {
		
		// You touched an object give player a point	
		GameController.pickedUp = GameController.pickedUp + 1;
		
		Debug.Log ("Pick Up");
		
		// Remove this gameobject attached to this script
		Destroy(this.gameObject);
	}
}

